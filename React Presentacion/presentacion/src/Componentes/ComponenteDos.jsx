export const ComponenteDos  = () => {
    return(
<div className="contenedor">
<div className="container">
    <div className="imagen">
        <img  class="foto" src="https://scontent.fcvj1-1.fna.fbcdn.net/v/t39.30808-6/330503727_3167062340250844_3839142172889075082_n.jpg?stp=cp6_dst-jpg_p526x296&_nc_cat=105&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeFIjh2u19SDnUZM5t4CVDozSQieu9tBO-VJCJ6720E75YsQTqq-LlKy-m_rfuAAVw0SLdd6xvmSw1QEuhBPiz5R&_nc_ohc=lSMEWtpz-9IAX_oPoJh&_nc_ht=scontent.fcvj1-1.fna&oh=00_AfCR-RtqjjM9mF6X0B7UKgVyitjAjrGJaPXtGUISkbTexQ&oe=640B63F6" alt="" />
    </div>
    <div className="datos">
    <h2>Campus: TECNOLOGICO DE CUAUTLA</h2>
            <h2>Carrera: ING.SISTEMAS COMPUTACIONALES</h2>
            <h2>N.Control: 18680171</h2>
            <h2>Recidencia: Yautepec Morelos</h2>
            <h2>Pasatiempos: LEER,VIAJAR,ESCUCHARMUSICA</h2>
            <h2>Correo: rl573211@gmail.com</h2>
    </div>
    <div class="redes">
        <h3>Contactar</h3>
        <ul class="social">
            <li><a href="https://www.facebook.com/yamir.garsialopez"><p class="he"><svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-facebook" width="52" height="52" viewBox="0 0 24 24" stroke-width="2" stroke="#00abfb" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                <path d="M7 10v4h3v7h4v-7h3l1 -4h-4v-2a1 1 0 0 1 1 -1h3v-4h-3a5 5 0 0 0 -5 5v2h-3" />
              </svg></p></a></li>
            <li><a href="https://instagram.com/rogrigos?igshid=ZDdkNTZiNTM=" target="_blank"><p class="ho"><svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-instagram" width="52" height="52" viewBox="0 0 24 24" stroke-width="2" stroke="#fd0061" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                <rect x="4" y="4" width="16" height="16" rx="4" />
                <circle cx="12" cy="12" r="3" />
                <line x1="16.5" y1="7.5" x2="16.5" y2="7.501" />
              </svg></p></a></li>
            <li><a href="https://twitter.com/RodrigoMejaLpe1"><p class="hu"><svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-twitter" width="52" height="52" viewBox="0 0 24 24" stroke-width="2" stroke="#00abfb" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                <path d="M22 4.01c-1 .49 -1.98 .689 -3 .99c-1.121 -1.265 -2.783 -1.335 -4.38 -.737s-2.643 2.06 -2.62 3.737v1c-3.245 .083 -6.135 -1.395 -8 -4c0 0 -4.182 7.433 4 11c-1.872 1.247 -3.739 2.088 -6 2c3.308 1.803 6.913 2.423 10.034 1.517c3.58 -1.04 6.522 -3.723 7.651 -7.742a13.84 13.84 0 0 0 .497 -3.753c-.002 -.249 1.51 -2.772 1.818 -4.013z" />
              </svg></p></a></li>
              <li><a href="https://www.snapchat.com/add/rodrigomejial23?share_id=jkCyhNDQR0E&locale=es-MX"><p class="ha''''-----"><svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-snapchat" width="52" height="52" viewBox="0 0 24 24" stroke-width="2" stroke="#ffec00" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"/> 
                <path d="M16.882 7.842a4.882 4.882 0 0 0 -9.764 0c0 4.273 -.213 6.409 -4.118 8.118c2 .882 2 .882 3 3c3 0 4 2 6 2s3 -2 6 -2c1 -2.118 1 -2.118 3 -3c-3.906 -1.709 -4.118 -3.845 -4.118 -8.118zm-13.882 8.119c4 -2.118 4 -4.118 1 -7.118m17 7.118c-4 -2.118 -4 -4.118 -1 -7.118" />
              </svg>    </p></a></li>
        </ul>

    </div>
</div>
</div> 
    )
}

